-- SQL-GBA-LEG-Tabellen-Skript
-- Autor: heghor
-- Datum: 2007.10.22
-- letzte �nderung: 
--   2011.09.07, heghor
--   2013.05.15, heghor
--   2013.10.14, heghor
--   2014.09.22, heghor: LEG_SYMBOL
--   2014.09.22, heghor: LEG_SYMBOL
--   2014.11.03, heghor: LEG_LEGENDE
--   2015.10.12, heghor: LEG_LEGENDE (stower: EN-Attribute)
--   2016.06.29, heghor: LEG_LEGENDE
--   2017.01.30, heghor: LEG_LEGENDE_GENLEG (Mappingtabelle)
--   2017.02.01, heghor: LEG_LEGENDE_GENLEG
--   2019.01.04, heghor: LEG_LEGENDE
test
--
-- Name: LEG.01.cre_tables.sql
-- create tables, als Benutzer GBA ausf�hren!!

-- 2013.05.15, heghor: ID => IDENTITY-Spalte!! FEATURES_ID => FEATURE_ID ge�ndert!
-- 2013.10.14, heghor: LEG und LEG_EN nvarchar(500) hinzugef�gt
-- 2014.11.03, heghor: LOKAL_ID int null, LOKAL_DS varchar(200) null hinzugef�gt
-- 2015.10.11, heghor, stower: EN-Attribut hinzugef�gt
-- 2016.06.29, heghor: TEKTONIK_ID int NULL hinzugef�gt
-- 2019.01.04, heghor: INAKTIV auf DEFAULT 0!
CREATE TABLE gba.LEG_LEGENDE (
	ID               int NOT NULL IDENTITY (1, 1),
	NR               varchar(20) NULL,
	FEATURE_ID       int NOT NULL,
	IDX              tinyint NULL,
	DRUCK_ALIAS      varchar(20) NULL,
	LEGPOS           varchar(20) NULL,
	LEGSYMART        varchar(10) NULL,
	LEGSYM           int NULL,
	LEGGROUP         varchar(7) NULL,
	LEGGRAPHICS      varchar(5) NULL,
	LEGSORT          varchar(20) NULL,
	SORTIERUNG       int NULL,
	INAKTIV          bit NOT NULL CONSTRAINT LEG_LEGENDE_df_INAKTIV DEFAULT (0),
	LEGENDENTEXT     varchar(500) NOT NULL,
	LEGENDENTEXT_N   nvarchar(500) NULL,
	KURZLEGENDE      varchar(200) NULL,
	UEBERSCHRIFT1    varchar(255) NULL,
	UEBERSCHRIFT2    varchar(255) NULL,
	UEBERSCHRIFT3    varchar(255) NULL,
	UEBERSCHRIFT4    varchar(255) NULL,
	UEBERSCHRIFT5    varchar(255) NULL,
	LITHOLOGIE       varchar(255) NULL,
	FARBE            varchar(60) NULL,
	GENESE           varchar(200) NULL,
	KLAMMER1         varchar(255) NULL,
	KLAMMER2         varchar(255) NULL,
	KLAMMER3         varchar(255) NULL,
	ALTER_TXT        varchar(255) NULL,
	ALTER_MAX        varchar(60) NULL,
	ALTER_MIN        varchar(60) NULL,
	ALTER_STUFE      varchar(255) NULL,
	EPOCHE_SERIE     varchar(255) NULL,
	PERIODE_SYSTEM   varchar(255) NULL,
	AERA_GRUPPE      varchar(255) NULL,
	TEKTONIK1        varchar(255) NULL,
	TEKTONIK1A       varchar(255) NULL,
	TEKTONIK2        varchar(255) NULL,
	TEKTONIK3        varchar(255) NULL,
	TEKTONIK4        varchar(255) NULL,
	GRUPPE           varchar(200) NULL,
	SUBGRUPPE        varchar(200) NULL,
	FORMATION        varchar(200) NULL,
	MEMBER           varchar(200) NULL,
	NOTIZ            varchar(255) NULL,
	E_USER           varchar(20) NULL,
	E_DATUM          smalldatetime NULL,
	A_USER           varchar(20) NULL,
	A_DATUM          smalldatetime NULL,
	T1               varchar(50) NULL,
	T2               varchar(255) NULL,
	T3               varchar(500) NULL,
	Z1               int NULL,
	LEG              nvarchar(500) NULL,
	LEG_EN           nvarchar(500) NULL,
	LOKAL_ID         int NULL,
	LOKAL_DS         varchar(200) NULL,
	UEBERSCHRIFT1_EN    varchar(255) NULL,
	UEBERSCHRIFT2_EN    varchar(255) NULL,
	UEBERSCHRIFT3_EN    varchar(255) NULL,
	UEBERSCHRIFT4_EN    varchar(255) NULL,
	UEBERSCHRIFT5_EN    varchar(255) NULL,
	LITHOLOGIE_EN       varchar(255) NULL,
	ALTER_TXT_EN        varchar(255) NULL,
	KLAMMER1_EN         varchar(255) NULL,
	KLAMMER2_EN         varchar(255) NULL,
	KLAMMER3_EN         varchar(255) NULL,
	TEKTONIK_ID         int NULL
)
GO

/* zum L�schen, wenn System auf LEG_SYMBOL umgestellt wurde! */
CREATE TABLE gba.LEG_FARBTAB (
	ID               int NOT NULL,
	FILL_SYMBOL      varchar(14) NULL,
	LINE_SYMBOL      varchar(10) NULL,
	MARKER_SYMBOL    varchar(14) NULL,
	R                smallint NULL,
	G                smallint NULL,
	B                smallint NULL,
	C                smallint NULL,
	M                smallint NULL,
	Y                smallint NULL,
	K                smallint NULL,
	NOTIZ            varchar(200) NULL,
	SYMBOL_TXT       varchar(20) NULL,
	SYMBOL_DATEI     varchar(200) NULL,
	ARCGIS_STYLE     varchar(50) NULL,
	MASSTAB          int NULL,
	E_USER           varchar(20) NULL,
	E_DATUM          datetime NULL,
	A_USER           varchar(20) NULL,
	A_DATUM          datetime NULL,
	T1               varchar(255) null,
	T2               varchar(255) null,
	Z1               int null,
	FILL_FARBCODE    varchar(4) NULL,
	FILL_MUSTERCODE  varchar(7) NULL,
	FILL_MUSTERFARBE varchar(3) NULL,
	LINE_MUSTERCODE  varchar(7) NULL,
	LINE_MUSTERFARBE varchar(3) NULL,
	MARK_MUSTERCODE  varchar(7) NULL,
	MARK_MUSTERFARBE varchar(3) NULL
)
GO

-- 2010.12.03, heghor: Neuanlage
-- 2014.09.22, heghor: ComputedColumns hinzugef�gt (FILL_SYMBOL, LINE_SYMBOL, MARKER_SYMBOL)
CREATE TABLE gba.LEG_SYMBOL (
	ID               int IDENTITY(1,1) NOT NULL,
	SYMBOL_TXT       varchar(20) NULL,
	SYMBOL_DATEI     varchar(255) NULL,
	ARCGIS_STYLE     varchar(50) NULL,
	FILL_FARBCODE    varchar(4) NULL,
	FILL_MUSTERCODE  varchar(7) NULL,
	FILL_MUSTERFARBE varchar(3) NULL,
	LINE_MUSTERCODE  varchar(7) NULL,
	LINE_MUSTERFARBE varchar(3) NULL,
	MARK_MUSTERCODE  varchar(7) NULL,
	MARK_MUSTERFARBE varchar(3) NULL,
	NOTIZ            nvarchar(255) NULL,
	MASSSTAB         int NULL,
	E_USER           varchar(20) NULL,
	E_DATUM          datetime NULL,
	A_USER           varchar(20) NULL,
	A_DATUM          datetime NULL,
	T1               varchar(2000) null,
	T2               varchar(255) null,
	Z1               int null,
 	FILL_SYMBOL      AS (case (isnull([FILL_FARBCODE],'')+isnull([FILL_MUSTERCODE],''))+isnull([FILL_MUSTERFARBE],'') when '' then '#' else (isnull([FILL_FARBCODE],'')+isnull([FILL_MUSTERCODE],''))+isnull([FILL_MUSTERFARBE],'') end) PERSISTED,
	LINE_SYMBOL      AS (case isnull([LINE_MUSTERCODE],'')+isnull([LINE_MUSTERFARBE],'') when '' then '#' else isnull([LINE_MUSTERCODE],'')+isnull([LINE_MUSTERFARBE],'') end) PERSISTED,
	MARKER_SYMBOL    AS (case isnull([MARKER_MUSTERCODE],'')+isnull([MARKER_MUSTERFARBE],'') when '' then '#' else isnull([MARKER_MUSTERCODE],'')+isnull([MARKER_MUSTERFARBE],'') end) PERSISTED
)
GO

-- 2009.07.15, reijoh, heghor: Umbenannt nach LEG_FARBCODE (fr�her LEG_FARBCODES)
-- 2009.07.15, reijoh, heghor: ID gel�scht und FARBCODE (ehemals vc(255)) auf varchar(4) ge�ndert => PK
-- 2011.09.07, heghor: C,M,Y,K,R,G,B auf tinyint ge�ndert (CMYK=float, RGB=smallint)
CREATE TABLE gba.LEG_FARBCODE (
	FARBCODE varchar(4) NOT NULL,
	C        tinyint NULL,
	M        tinyint NULL,
	Y        tinyint NULL,
	K        tinyint NULL,
	R        tinyint NULL,
	G        tinyint NULL,
	B        tinyint NULL,
	LL       float null,
	AA       float null,
	BB       float null,
	E_USER   varchar(25) null,
	E_DATUM  datetime null,
	A_USER   varchar(25) null,
	A_DATUM  datetime null	
)
GO

-- 2009.07.15, reijoh, heghor: Neuanlage
create table gba.LEG_MUSTERCODE (
	MUSTERCODE        varchar(7) not null,
	MUSTERCODE_BEZ    varchar(50) null,
	MUSTERCODE_TYPE   varchar(255) null,
	MUSTERCODE_GRUPPE varchar(50) null,
	NOTIZ             varchar(255) null,
	E_USER            varchar(25) null,
	E_DATUM           datetime null,
	A_USER            varchar(25) null,
	A_DATUM           datetime null	
)
GO

-- 2009.07.15, reijoh, heghor: Neuanlage
-- 2011.09.07, heghor: C,M,Y,K,R,G,B tinyint hinzugef�gt
create table gba.LEG_MUSTERFARBE (
	MUSTERFARBE     varchar(3) not null,
	MUSTERFARBE_BEZ varchar(50) null,
	NOTIZ           varchar(255) null,
	E_USER          varchar(25) null,
	E_DATUM         datetime null,
	A_USER          varchar(25) null,
	A_DATUM         datetime null,
	C        tinyint NULL,
	M        tinyint NULL,
	Y        tinyint NULL,
	K        tinyint NULL,
	R        tinyint NULL,
	G        tinyint NULL,
	B        tinyint NULL
)
GO

/* ********************************************** */
-- LEG_LEGENDE - GENLEG-Mapping-Tabelle
-- 1:1 �ber LEG_ID <=> LEG_LEGENDE.ID
-- 2017.01.30, heghor: Neuanlage
-- 2017.02.01, heghor: ERLEDIGT bit => ERLEDIGT varchar(10)!
--
CREATE TABLE [gba].[LEG_LEGENDE_GENLEG](
  LEG_ID              int NOT NULL,
  GENLEG_ID_quartaer  int NULL,
  GENLEG_ID_geomorph  int NULL,
  GENLEG_ID_phaenomen int NULL,
  GENLEG_ID_tektonik  int NULL,
  ERLEDIGT            varchar(10) null,
  NOTIZ               varchar(255) NULL,
  E_USER              varchar(15) NULL,
  E_DATUM             datetime NULL,
  A_USER              varchar(15) NULL,
  A_DATUM             datetime NULL
)
GO
